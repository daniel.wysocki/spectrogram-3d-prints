# Spectrogram 3D Prints

This is a tool for producing 3D printer files (.stl) for spectrograms of LIGO data. It is mostly a convenience wrapper around the Python library [`stl_tools`](https://github.com/thearn/stl_tools).


## Installation

Clone the repository and run

```bash
pip install -U .
```


## Credits

Copyright 2018 - Daniel Wysocki
