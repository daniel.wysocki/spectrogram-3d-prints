class MissingFieldNameError(Exception):
    def __init__(self, format_type):
        message = "'field_name' needed for format '{}'".format(format_type)
        super(MissingFieldNameError, self).__init__(message)

def load_general(filename, field_name):
    extension = filename.split(".")[-1]

    return parsers[extension](filename, field_name)


def load_txt(filename, field_name):
    import numpy

    return numpy.loadtxt(filename)


def load_hdf5(filename, field_name):
    if field_name is None:
        raise MissingFieldNameError("hdf5")

    import h5py

    with h5py.File(filename, "r") as f:
        return f[field_name].value


def load_matlab(filename, field_name):
    if field_name is None:
        raise MissingFieldNameError("matlab")

    import scipy.io

    return scipy.io.loadmat(filename)[field_name]


parsers = {
    "hdf5" : load_hdf5,
    "h5" : load_hdf5,
    "dat" : load_txt,
    "txt" : load_txt,
    "asc" : load_txt,
    "mat" : load_matlab,
}

supported_extensions = list(parsers)
