from __future__ import print_function


def get_args(raw_args):
    import argparse

    from .io import supported_extensions

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "spectrogram_data",
        help="File containing spectrogram data. "
             "Supports extensions: {}".format(" ".join(supported_extensions)),
    )
    parser.add_argument(
        "stl_output",
        help=".stl file to output model to.",
    )

    parser.add_argument(
        "--length-width",
        type=float, nargs=2, default=(64, 120)
    )
    parser.add_argument(
        "--depth",
        type=float, default=0.1,
    )

    parser.add_argument(
        "--gaussian-smoothing",
        type=float,
        help="Apply Gaussian kernel smoothing to the model, with a given "
             "standard deviation.",
    )

    parser.add_argument(
        "--surface-only",
        action="store_false", dest="solid",
        help="Make the 3D model include only the surface, with no volume. "
             "This should not be used for printing, and only for inspecting "
             "the shape of the model on a computer.",
    )

    parser.add_argument(
        "--input-field-name",
        help="If using a spectrogram format with field names (e.g., hdf5), "
             "you must provide the name of that field here.",
    )

    return parser.parse_args(raw_args)


def main(raw_args=None):
    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    from . import io, stl

    args = get_args(raw_args)

    length, width = args.length_width

    # Load spectrogram data.
    data = io.load_general(args.spectrogram_data, args.input_field_name)

    # Create STL file.
    stl.make_stl(
        data,
        args.stl_output,
        length, width, args.depth,
        args.gaussian_smoothing,
        args.solid,
    )
