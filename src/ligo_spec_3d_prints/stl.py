from __future__ import division, print_function


def make_stl(data, filename, length, width, depth, smoothing, solid):
    import numpy
    import stl_tools
    import scipy.ndimage
    import skimage.transform

    # Flip image
    data = numpy.flipud(data)
    # Resize image
    data = skimage.transform.resize(
        data, (length, width),
        mode="constant", preserve_range=True,
    )
    # Apply smoothing
    if smoothing is not None:
        data = scipy.ndimage.gaussian_filter(data, smoothing)

    # Create (and save) STL file
    stl_tools.numpy2stl(data, filename, scale=depth, solid=solid)
