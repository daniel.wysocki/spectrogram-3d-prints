"""Spectrogram 3D Prints

Code for creating .stl files to 3D print LIGO spectrograms.
"""

from datetime import date


#-------------------------------------------------------------------------------
#   GENERAL
#-------------------------------------------------------------------------------
__name__        = "pop_models"
__version__     = "0.1.0a1"
__date__        = date(2018, 3, 21)
__keywords__    = [
    "gravitational waves",
    "data analysis",
    "3D printing",
    "physics",
]
__status__      = "Alpha"


#-------------------------------------------------------------------------------
#   URLS
#-------------------------------------------------------------------------------
__url__         = "https://git.ligo.org/daniel.wysocki/spectrogram-3d-prints"
__bugtrack_url__= "https://git.ligo.org/daniel.wysocki/spectrogram-3d-prints/issues"


#-------------------------------------------------------------------------------
#   PEOPLE
#-------------------------------------------------------------------------------
__author__      = "Daniel Wysocki"
__author_email__= "daniel.wysocki@ligo.org"

__maintainer__      = "Daniel Wysocki"
__maintainer_email__= "daniel.wysocki@ligo.org"

__credits__     = ("Daniel Wysocki",)


#-------------------------------------------------------------------------------
#   LEGAL
#-------------------------------------------------------------------------------
__copyright__   = 'Copyright (c) 2018 {author} <{email}>'.format(
    author=__author__,
    email=__author_email__
)

__license__     = 'MIT'
__license_full__= '''
Licensed under MIT License
https://opensource.org/licenses/MIT
'''.strip()


#-------------------------------------------------------------------------------
#   PACKAGE
#-------------------------------------------------------------------------------
DOCLINES = __doc__.split("\n")

CLASSIFIERS = """
Development Status :: 3 - Alpha
Programming Language :: Python
Programming Language :: Python :: 2
Programming Language :: Python :: 3
Operating System :: OS Independent
Intended Audience :: Science/Research
Topic :: Scientific/Engineering :: Astronomy
Topic :: Scientific/Engineering :: Physics
""".strip()

REQUIREMENTS = {
    "install": [
        "numpy>=1.0.0",
        "scipy>=1.0.0",
        "scikit-image>=0.13.0",
        "h5py>=2.4.0",
        "six>=1.10.0",
        "stl_tools>=0.3.0",
    ],
    "tests": [
    ]
}

ENTRYPOINTS = {
    "console_scripts" : [
        "ligo_spectrogram_3d_print = ligo_spec_3d_prints.cli:main"
    ]
}

from setuptools import find_packages, setup

metadata = dict(
    name        =__name__,
    version     =__version__,
    description =DOCLINES[0],
    long_description='\n'.join(DOCLINES[2:]),
    keywords    =__keywords__,

    author      =__author__,
    author_email=__author_email__,

    maintainer  =__maintainer__,
    maintainer_email=__maintainer_email__,

    url         =__url__,
#    download_url=__download_url__,

    license     =__license__,

    classifiers=[f for f in CLASSIFIERS.split('\n') if f],

    package_dir ={"": "src"},
    packages    =[
        "ligo_spec_3d_prints",
    ],
#    packages=find_packages(exclude=['tests', 'tests.*']),

    install_requires=REQUIREMENTS["install"],
#    tests_require=REQUIREMENTS["tests"],

    entry_points=ENTRYPOINTS
)

setup(**metadata)
